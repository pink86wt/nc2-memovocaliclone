//
//  ViewController.swift
//  MemoVocaliClone
//
//  Created by Mirella Cetronio on 15/12/2019.
//  Copyright © 2019 Mirella Cetronio. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVAudioRecorderDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    
    var toggleState = 1
    var numberOfRecords: Int = 0
    
    @IBOutlet weak var buttonLabel: UIButton!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    
    @IBAction func playTapped(_ sender: UIButton) {
        if toggleState == 1 {
            audioPlayer.play()
            toggleState = 2
            playButton.setImage(UIImage(named:"pause.png"),for:UIControl.State.normal)
        } else {
            audioPlayer.pause()
            toggleState = 1
            playButton.setImage(UIImage(named:"play.png"),for:UIControl.State.normal)
        }
    }
    
    @IBAction func stopTapped(_ sender: UIButton) {
        audioPlayer.stop()
    }
    
    @IBAction func record(_ sender: Any) {
        //check if already recording
        if audioRecorder == nil {
            numberOfRecords += 1
            let fileName = getDirectory().appendingPathComponent("\(numberOfRecords).m4a")
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey: 12000, AVNumberOfChannelsKey: 1, AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
            
            //start recording
            do {
                audioRecorder = try AVAudioRecorder(url: fileName, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
                
//                buttonLabel.setTitle("Stop Recording", for: .normal)
                buttonLabel.setImage(UIImage(named: "pause"), for: .normal)
                
            } catch {
                displayAlert(title: "Ooops!", message: "Recording failed")
            }
        } else {
            //stop recording
            audioRecorder.stop()
            audioRecorder = nil
            
            UserDefaults.standard.set(numberOfRecords, forKey: "myNumber")
            myTableView.reloadData()
            
//            buttonLabel.setTitle("Record", for: .normal)
            buttonLabel.setImage(UIImage(named: "rec"), for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        playButton.alpha = 0
        stopButton.alpha = 0
        
        recordingSession = AVAudioSession.sharedInstance()
        
        if let number: Int = UserDefaults.standard.object(forKey: "myNumber") as? Int {
            numberOfRecords = number
        }
        
        AVAudioSession.sharedInstance().requestRecordPermission { (hasPermission) in
            if hasPermission {
                print("Accepted")
            }
        }
    }
    
    //gets path to the directory
    func getDirectory() -> URL {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = path[0]
        return documentDirectory
    }
    
    //display an alert
    func displayAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "dismiss", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    //setting up tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRecords
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = String("Audio \(indexPath.row + 1)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let path = getDirectory().appendingPathComponent("\(indexPath.row + 1).m4a")
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: path)
            playButton.alpha = 1
            stopButton.alpha = 1
//            audioPlayer.play()
        } catch {
        }
    }
    
}

